import React, { Component } from "react";
import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import "./Login.css";
import axios from "axios";
import Header from "../components/header"


class LoginComponent extends Component {
    constructor(props) {
        super(props);
        console.log(props)
        this.validateForm = this.validateForm.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = {
            loggedInStatus: this.props.state.loggedInStatus,
            username: '',
            password: ''
        };

    }
    componentDidMount (){
        if (this.state.loggedInStatus === "LOGGED_IN"){
            this.props.history.push("/");
        }
    }

    setPassword(event) {
        this.setState({ password: event.target.value })
    }

    setUsername(event) {
        this.setState({ username: event.target.value })
    }

    validateForm() {
        return this.state.password.length > 0 && this.state.username.length > 0;
    }

    handleSubmit(event) {
        event.preventDefault();
        const user = {
            email: this.state.email,
            password: this.state.password,
            username: this.state.username,
        }
        axios.post('http://localhost:5000/api/sessions', user)
            .then(response => {
                console.log(response);
                if (response.data.status === "success") {
                    this.props.handleSuccessfulLogin(response.data);
                }
            })
            .catch(error => {
                console.log(error);
            });
    }

    render() {
        console.log('I was triggered during render')
        return (
            <div>
            <Header state={this.props.state}></Header>
            <div className="Login">
                <form onSubmit={this.handleSubmit}>
                    <FormGroup controlId="username" bsSize="large">
                        <ControlLabel>Username</ControlLabel>
                        <FormControl
                            autoFocus
                            type="text"
                            value={this.state.username}
                            onChange={e => this.setUsername(e)}
                        />
                    </FormGroup>
                    <FormGroup controlId="password" bsSize="large">
                        <ControlLabel>Password</ControlLabel>
                        <FormControl
                            value={this.state.password}
                            onChange={e => this.setPassword(e)}
                            type="password"
                        />
                    </FormGroup>
                    <Button block bsSize="large" disabled={!this.validateForm()} type="submit">
                        Login
                    </Button>
                </form>
            </div>
            </div>
        );
    }
}

export default LoginComponent;

