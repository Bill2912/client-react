
import React, { Component } from "react";

export default class Header extends Component {
    constructor(props){
        super(props)
        this.state = this.props.state;
    }
    render() {
        return (
        <header className="blog-header py-3">
            <div className="row flex-nowrap justify-content-between align-items-center">
                <div className="col-4 pt-1">
                    <a className="text-muted" href="/login/form">
                        LOG IN
                </a>
                </div>
            </div>

            <h3>Status: {this.state.loggedInStatus}</h3>

            <div className="flex-nowrap justify-content-between align-items-center mt-3">
                <nav className="navbar navbar-expand-md">
                    <div
                        className="row collapse navbar-collapse"
                        id="navbarCollapse"
                    >
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item btn btn-light">
                                <a className="navbar-brand mb-0 h1" href="/home">
                                    Movies List <span className="sr-only">(current)</span>
                                </a>
                            </li>
                            <li className="nav-item btn btn-light">
                                <a className="navbar-brand mb-0 h1" href="/likedMovie">
                                    Favorite Movie
                                </a>
                            </li>
                            <li className="nav-item btn btn-light">
                                <a className="navbar-brand mb-0 h1" href="/recommend/movies">
                                    Recommended
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </header>
        );
    }
};
