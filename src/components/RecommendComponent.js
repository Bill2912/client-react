import React, { Component } from "react";
import Header from "../components/header";
import MovieItemComponent from "../components/MovieItemComponent";
import Footer from "../components/Footer";
import axios from "axios";

class Recommended extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listMovies: [
            ],
            user: this.props.state.user,
            loggedInStatus: this.props.state.loggedInStatus
        };
        this.showListMovieLiked = this.showListMovieLiked.bind(this);
    }

    componentWillMount() {
        var list = []
        axios.get("http://localhost:5000/api/script")
        .then(response => {
            var str_arr = response.data[1]
            str_arr = str_arr.substring(1, str_arr.length-1).split(",");
            str_arr.forEach((item) => {
                var movie_id = parseFloat(item)
                axios.get(`http://localhost:5000/api/get/movie/${movie_id}`)
                .then(res => {
                    list.push(res.data.movie)
                })
            })
        })
        .catch(error => {
            throw error;
        })
        this.setState({listMovies:list})
    }

    onChangeStatus = (id) => {
        console.log("movie id:" + id);
        this.state.listMovies.map((item, index) => {
            if (index === id) {
                item.isLiked = !item.isLiked;
                console.log("status: " + item.isLiked);
                this.state.listMovies[id] = item;
                this.setState({
                    listMovies: this.state.listMovies,
                });

                localStorage.setItem("list", JSON.stringify(this.state.listMovies));
            }
        });
    };
    showListMovieLiked(listMovies) {
        return (
            <div>
                {listMovies != null
                    ? listMovies.map((movie, index) => {
                            return (
                                <MovieItemComponent
                                    key={index}
                                    id={index}
                                    name={movie.name}
                                    description={movie.description}
                                    rateScore={movie.rateScore}
                                    actors={movie.actors}
                                    // releaseDate={movie.releaseDate}
                                    imageUrl={movie.imageUrl}
                                    isLiked={movie.isLiked}
                                    onUpdateStatus={this.onChangeStatus}
                                    onChange={this.onChangeStatus}
                                />
                            );
                    })
                    : ""}
            </div>
        );
    }
    render() {
        console.log(this.state.listMovies)
        return (
            <div>
                <Header state={this.props.state} />
                <div>
                    {this.showListMovieLiked(this.state.listMovies)}
                </div>
                <Footer />
            </div>
        );
    }
}

export default Recommended;
